
public class Persona {
	private final String nombre;
	private final long dni;
	
	public Persona(String nombre, long dni) {
		this.nombre = nombre;
		this.dni = dni;
	}

	public final String getNombre() {
		return nombre;
	}

	public final long getDni() {
		return dni;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Una persona llamada " + this.nombre;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Persona) {
			return this.dni == ((Persona) o).getDni();
		}
		return super.equals(o);
	}
}
