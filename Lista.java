
public class Lista {
	private Persona[] elementos; 
	private int contador;
	
	public Lista() {
		elementos = new Persona[100];
	}
	
	public void add(Persona e) {
		elementos[contador++] = e;
	}
	
	public Persona getElement (int index) {
		return elementos[index];
	}
	
	public int size() {
		return contador;
	}
	
	public static void main (String args[]) {
		Lista l = new Lista();
		Persona p2 = new Persona("Fede", 123123);
		l.add(p2);
		Object p4 = new Persona("Fededsafsd", 123123);
		Estudiante p3 = new Estudiante("Andres", 123123, 9876);
		l.add(p3);
		Object p = l.getElement(0);
		
		Estudiante p5 = new Estudiante("asadsa", 123123, 000);
		
		System.out.println(p3.equals(p5));
		
		
		//System.out.println(l.getElement(1));
		//System.out.println(l.size());
	}
}
