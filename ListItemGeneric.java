
public class ListItemGeneric<T> {
	private ListItemGeneric<T> next;
	private T item;
	
	public ListItemGeneric(T o) {
		item = o;
		next = null;
	}
	
	public ListItemGeneric<T> getNext() {
		return next;
	}
	public void setNext(ListItemGeneric<T> next) {
		this.next = next;
	}
	public T getItem() {
		return item;
	}
	public void setItem(T item) {
		this.item = item;
	}
}
