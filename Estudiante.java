
public class Estudiante extends Persona {
	
	protected final long legajo;

	public Estudiante(String nombre, long dni, long legajo) {
		super(nombre, dni);
		this.legajo = legajo;
	}

	public long getLegajo() {
		return legajo;
	}
	
	public String toString() {
		return "Un estudiante llamado " + getNombre(); 
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Estudiante) {
			return this.legajo == ((Estudiante) o).getLegajo();
		}
		return super.equals(o);
	}
}
