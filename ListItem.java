
public class ListItem {
	private ListItem next;
	private Object item;
	
	public ListItem(Object o) {
		item = o;
		next = null;
	}
	
	public ListItem getNext() {
		return next;
	}
	public void setNext(ListItem next) {
		this.next = next;
	}
	public Object getItem() {
		return item;
	}
	public void setItem(Object item) {
		this.item = item;
	}
}
