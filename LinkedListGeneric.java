
public class LinkedListGeneric<T> {
	ListItemGeneric<T> start;
	ListItemGeneric<T> current;
	ListItemGeneric<T> end;
	
	public LinkedListGeneric() {
		start = null;
		current = null;
	}
	
	public void add(T obj) {
		if (start == null) {
			// Lista vacia. Agrego al principio
			start = new ListItemGeneric<T>(obj);
			current = start;
			end = start;
		} else {
			// Agrego al final
			ListItemGeneric<T> aux = new ListItemGeneric<T>(obj);
			end.setNext(aux);
			end = aux;
		}
	}
	
	public T getNext() {
		if (current != null) {
			current = current.getNext();
			return (current == null) ? null : current.getItem();
		}
		return null;
	}

	public T getFirst() {
		return (start == null) ? null : start.getItem();
	}
}
