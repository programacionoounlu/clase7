
public class Mamifero extends Animal {
	private int litrosXDia;
	
	public int getLitrosXDia() {
		return litrosXDia;
	}

	public void setLitrosXDia(int litrosXDia) {
		this.litrosXDia = litrosXDia;
	}

	public Mamifero(String nombre, int litros) {
		super(nombre);
		litrosXDia = litros;
	}
}
