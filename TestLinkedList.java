
public class TestLinkedList {

	public static void main(String args[]) {
		LinkedListGeneric<Persona> lista;
		lista = new LinkedListGeneric<Persona>();
		LinkedListGeneric<Animal> lAnimal = new LinkedListGeneric<Animal>();
			
		Persona p = new Persona("Fede", 123);
		//lista.add((Object)p);
		lista.add(p);
		p = new Persona("Andres", 1234);
		lista.add(p);
		p = new Persona("Walter", 9999);
		lista.add(p);

		
		Animal a = new Animal("Perro");
		lAnimal.add(a);
		//lista.add(a);
		//lAnimal.add(p);
		
		// Idea de iterador
		System.out.println("Recorro lista");
		Persona o = lista.getFirst();
		while (o != null) {
			System.out.println("Mostrar el obj");
			System.out.println(o.toString());
			System.out.println(o.getDni());
			o = lista.getNext();
		}
	}
}
