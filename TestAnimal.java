
public class TestAnimal {

	public static void main(String args[]) {
		Animal a1 = new Mamifero("Vaca", 50);
		Animal a2 = new Mamifero("Oveja", 10);
		Animal a3 = new Reptil("Vibora");
		
		LinkedListGeneric<Animal> lista;
		lista = new LinkedListGeneric<Animal>();
		lista.add(a1);
		lista.add(a2);
		lista.add(a3);
		
		Animal aux = lista.getFirst();
		while (aux != null) {
			System.out.println(aux);
			aux = lista.getNext();
		}
	}
}
