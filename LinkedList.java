
public class LinkedList {
	ListItem start;
	ListItem current;
	ListItem end;
	
	public LinkedList() {
		start = null;
		current = null;
	}
	
	public void add(Object obj) {
		if (start == null) {
			// Lista vacia. Agrego al principio
			start = new ListItem(obj);
			current = start;
			end = start;
		} else {
			// Agrego al final
			ListItem aux = new ListItem(obj);
			end.setNext(aux);
			end = aux;
		}
	}
	
	public Object getNext() {
		if (current != null) {
			current = current.getNext();
			return (current == null) ? null : current.getItem();
		}
		return null;
	}

	public Object getFirst() {
		return (start == null) ? null : start.getItem();
	}
}
